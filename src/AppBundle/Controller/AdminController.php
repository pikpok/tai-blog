<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ornj\Bundle\MarkdownBundle\Form\Type\MarkdownType;

class AdminController extends Controller
{
    /**
     * @Route("/admin/", name="admin_home")
     */
    public function indexAction(Request $request)
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function listUsersAction(Request $request)
    {
      $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
      return $this->render('admin/users.html.twig', [
        'users' => $users
      ]);
    }
    
    /**
     * @Route("/admin/users/delete/{id}", name="admin_users_delete")
     */
    public function deleteUserAction($id, Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $user = $em->getRepository('AppBundle:User')->find($id);
      $em->remove($user);
      $em->flush();

      return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/posts", name="admin_posts")
     */
    public function listPostsAction(Request $request)
    {
      $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAll();
      return $this->render('admin/posts.html.twig', [
        'posts' => $posts
      ]);
    }

    /**
     * @Route("/admin/posts/delete/{id}", name="admin_posts_delete")
     */
    public function deletePostAction($id, Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $post = $em->getRepository('AppBundle:Post')->find($id);
      $em->remove($post);
      $em->flush();

      return $this->redirectToRoute('admin_posts');
    }

    /**
     * @Route("/admin/posts/edit/{id}", name="admin_posts_edit")
     */
    public function editPostAction($id, Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $post = $em->getRepository('AppBundle:Post')->find($id);
      
      $form = $this->createFormBuilder($post)
          ->add('title', TextType::class)
          ->add('content', MarkdownType::class)
          ->add('save', SubmitType::class, array('label' => 'Edytuj wpis'))
          ->getForm();
          
      $form->handleRequest($request);
          
      if ($form->isSubmitted() && $form->isValid()) {
        $post = $form->getData();
        $now = new\DateTime('now');
        $post->setDate($now);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('homepage');
      }
      
      return $this->render('admin/edit.html.twig', [
          'form' => $form->createView()
      ]);
    }

    /**
     * @Route("/admin/posts/new", name="admin_posts_new")
     */
    public function newPostAction(Request $request)
    {
        $post = new Post();

        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class)
            ->add('content', MarkdownType::class)
            ->add('save', SubmitType::class, array('label' => 'Dodaj wpis'))
            ->getForm();
            
        $form->handleRequest($request);
            
        if ($form->isSubmitted() && $form->isValid()) {
          $newPost = $form->getData();
          $now = new\DateTime('now');
          $newPost->setDate($now);
          $em = $this->getDoctrine()->getManager();
          $em->persist($newPost);
          $em->flush();

          $this->addFlash('notice', 'Dodano post');
          return $this->redirectToRoute('homepage');
        }
        
        return $this->render('admin/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
