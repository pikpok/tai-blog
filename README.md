tai-blog
====

Aplikacja stworzona na przedmiot "Tworzenie Aplikacji Internetowych"

_Krzysztof Bezrąk_

## Wymagania:

- środowisko zgodne z wymaganiami Symfony - 
  https://symfony.com/doc/current/reference/requirements.html (można sprawdzić 
  poprzez polecenie `php bin/symfony_requirements`)
- Composer - https://getcomposer.org/download/

## Uruchomienie aplikacji

- sklonować repozytorium (lub ściagnąć w formacie `.zip`)
- przejść w terminalu do katalogu głównego sklonowanego repozytorium
- uruchomić polecenie `composer install`
- skopiować plik `app/config/parameters.yml.dist` do `app/config/parameters.yml`
  i uzupełnić w nim dane bazy
- uruchomić migrację bazy danych poleceniem 
  `php bin/console doctrine:schema:update --force`

Dodatkowo można utworzyć użytkownika z dostępem super administratora:
```shell
$ php bin/console fos:user:create {login} {email} {hasło}`
$ php bin/console fos:user:promote {login} --super
```